import React from 'react';
import {
    ScrollView, Text, View, StyleSheet, Animated, TouchableOpacity, Dimensions, FlatList,
} from "react-native";
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import _ from 'lodash'
import {formatDate} from "./RenderPage";
import {db} from "../../firebase";
import { List, ListItem } from 'react-native-elements'
import XDate from 'xdate';

class Route extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [
            ],

        }
    }

    componentDidMount(){
        let d = XDate(); //current date eg. "2018-06-06T16:54:42.099Z"
        let m = d.getMonth()+1; // get month (0-11) +1 = (1-12) eg. 6
        let y = d.getFullYear(); //get year eg. 2018
        let academicYear = '';
        if(m>=9){
            academicYear = y+'-'+y+1;
        }
        else{
            academicYear = y-1+'-'+y;
        }
        var temp = []
        db.ref('AcademicYear/'+academicYear+'/'+this.props.term+"/")
            .on('value', snapshot => {
                snapshot.forEach(element => {
                    // console.log(element)
                    let description = element.val()['description']
                    let date = element.val()['startDate']

                    let titles = {}
                    titles['name'] = description
                    titles['subtitle'] = formatDate(new Date(date))
                    titles['other'] = new Date(date)
                    temp.push(titles)
                })
                this.state.items = temp
                this.setState({items: _.sortBy(this.state.items, 'other')})
            })
    }

    render(){
            return(
                <ScrollView>
                    <List >
                        {
                            this.state.items.map((l, i) => (
                                <ListItem
                                    titleStyle={{ color: '#0e4790', fontWeight: 'bold'}}
                                    hideChevron={true}
                                    key={i}
                                    title={l.name}
                                    subtitle={l.subtitle}
                                />
                            ))
                        }
                    </List>
                </ScrollView>
            )
    }
}



const FirstRoute = () => (
    <Route term="1"/>

)

const SecondRoute = () => (
    <Route term="2"/>
);



const ThirdRoute = () => (
    <Route term="3"/>

)
const Summer = () => (
    <Route term="Summer"/>

)



export default class ListAll extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            routes: [
                { key: 'first', title: 'Term 1' },
                { key: 'second', title: 'Term 2' },
                { key: 'third', title: 'Term 3' },
                { key: 'fourth', title: 'Summer' },
            ],
        }
    }

    _handleIndexChange = index => this.setState({ index });


    render(){

        return(

            <View>
                    <View style={{
                        height:'100%'}}>
                        <TabView

                            key = {0}
                            navigationState={this.state}
                            onIndexChange={index => this.setState({ index })}
                            renderScene={SceneMap({
                                first:FirstRoute,
                                second: SecondRoute,
                                third: ThirdRoute,
                                fourth: Summer

                            })}
                            renderTabBar={props =>
                                <TabBar
                                    {...props}
                                    indicatorStyle={{
                                        backgroundColor: '#fcb913',
                                    }}
                                    style={{
                                        backgroundColor: '#0e4790',
                                        paddingTop: 20

                                    }}
                                />
                            }
                            initialLayout={{ height: 0,
                                width: Dimensions.get('window').width }}
                        />
                    </View>
            </View>
        )
    }
}

