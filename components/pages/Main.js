import * as React from 'react';
import { ScrollView, StyleSheet, Text, View, Dimensions, StatusBar, Image, TouchableOpacity, Animated } from 'react-native';
import Tabbar from 'react-native-tabbar-bottom'
import ListAll from "./ListAll";
import Countdown from "./Countdown";
import Home from "./Home";
import AgendaScreen from "./AgendaScreen";

export default class Main extends React.Component {
    constructor() {
        super()
        this.state = {
            page: "CountDown",
        }
    }

    render() {
        return (
            <View style={styles.container}>

                {this.state.page === "CountDown" && <Countdown/>}
                {this.state.page === "HomeScreen" && <Home/>}
                {this.state.page === "ListAll" && <ListAll/>}
                {this.state.page === "AgendaScreen" && <AgendaScreen/>}

                 <Tabbar
                    rippleEffect={false}
                    tabbarBorderTopColor={"#e1e1e1"}
                    selectedIconColor={"#0e4790"}
                    labelColor={"#3F3F3F"}
                    selectedLabelColor={"#0e4790"}
                    tabbarBgColor={"#f9f9f9"}
                    iconColor={"#3F3F3F"}
                    stateFunc={(tab) => {
                        this.setState({page: tab.page})
                    }}
                    activePage={this.state.page}
                    tabs={[

                        {
                            page: "HomeScreen",
                            icon: "calendar",
                            iconText: "Calendar"
                        },
                        {
                            page: "CountDown",
                            icon: "stopwatch",
                            iconText: "CountDown"


                        },
                        {
                            page: "ListAll",
                            icon: "list",
                            iconText: "ListAll"
                        },

                    ]}
                 />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    }
});