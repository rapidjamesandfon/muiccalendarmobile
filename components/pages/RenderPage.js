import React from 'react';
import {
    ScrollView, Text, View, StyleSheet, Animated, TouchableOpacity, Dimensions, FlatList,
} from "react-native";

import {Count} from "./Countdown";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import {db} from "../../firebase";
import Spinner from 'react-native-loading-spinner-overlay';
import XDate from 'xdate';

export function formatDate(date) {
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

export default class RenderPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            instructionBegin:-1,
            registration: -1,
            paymentDueDate: -1,
            midtermExam:-1,
            finalExam: -1,
            date1: "",
            date2: "",
            date3: "",
            date4: "",
            date5: "",


        }
    }

    componentDidMount(){
        let d = XDate(); //current date eg. "2018-06-06T16:54:42.099Z"
        let m = d.getMonth()+1; // get month (0-11) +1 = (1-12) eg. 6
        let y = d.getFullYear(); //get year eg. 2018
        let academicYear = '';
        if(m>=9){
            academicYear = y+'-'+y+1;
        }
        else{
            academicYear = y-1+'-'+y;
        }

        var now = (new Date()).getTime()/1000
        db.ref('AcademicYear/'+academicYear+'/'+this.props.term+"/")
            .on('value', snapshot => {
                snapshot.forEach(element => {
                    let description = element.val()['description']
                    let date = element.val()['startDate']

                    if (((new Date(date).getTime()/1000)-now) < 0){
                        var newDate = 0

                    }
                    else {
                        var newDate = (new Date(date).getTime()/1000)-now
                    }

                    if (description === 'Current students’ registration') {

                        this.setState({registration: newDate,date1: formatDate(new Date(date)) })
                    }
                    else if (description === 'Instruction Begin') {
                        this.setState({ instructionBegin:newDate, date2: formatDate(new Date(date))})

                    }
                    else if (description === 'Payment due date') {
                        this.setState({paymentDueDate: newDate, date3: formatDate(new Date(date)) })

                    }
                    else if (description === "Mid-term examination dates" ){
                        this.setState({midtermExam:newDate,date4: formatDate(new Date(date)) })
                    }
                    else if (description === "Final examination dates"){
                        this.setState({finalExam: newDate,date5: formatDate(new Date(date)) })
                    }
                })

            })
    }

    render(){

        if (this.state.instructionBegin === -1 || this.state.paymentDueDate === -1
            || this.state.registration === -1 || this.state.finalExam === -1 ||
            this.state.midtermExam === -1){
            return (
                <View style={{ flex: 1 }}>
                    <Spinner visible={true} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
                </View>
            )
        }
        else{

            return (
                <ScrollView>

                    <Count icon={<SimpleLineIcons size={35} color="black" name="cloud-upload"> </SimpleLineIcons>}
                    title={"Registration"}
                    date={this.state.date1}
                    until={this.state.registration}/>

                    <Count icon={<SimpleLineIcons size={35} color="black" name="briefcase"> </SimpleLineIcons>}
                           title={"Instruction Begin"}
                           date={this.state.date2}
                           until={this.state.instructionBegin}/>

                    <Count icon={<SimpleLineIcons size={35} color="black" name="credit-card"> </SimpleLineIcons>}
                    title={"Payment Due Date"}
                    date={this.state.date3}
                    until={this.state.paymentDueDate}/>

                    <Count icon={<SimpleLineIcons size={35} color="black" name="book-open"> </SimpleLineIcons>}
                    title={"Midterm"}
                    date={this.state.date4}
                    until={this.state.midtermExam}/>

                    <Count icon={<SimpleLineIcons size={35} color="black" name="book-open"> </SimpleLineIcons>}
                    title={"Final examination Date"}
                    date={this.state.date5}
                    until={this.state.finalExam}/>

                </ScrollView>
            )
        }

    }
}
