import React from 'react';
import {
    ScrollView, Text, View, StyleSheet, Animated, TouchableOpacity, Dimensions, FlatList,
} from "react-native";

import {Count} from "./Countdown";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import {db} from "../../firebase";

import Spinner from 'react-native-loading-spinner-overlay';
import {formatDate} from "./RenderPage";
import XDate from 'xdate';

export default class SummerTerm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            instructionBegin:-1,
            registration: -1,
            paymentDueDate: -1,
            // midtermExam:0,
            finalExam: -1,
            date1: "",
            date2: "",
            date3: "",
            date4: "",
        }
    }

    componentDidMount(){
        let d = XDate(); //current date eg. "2018-06-06T16:54:42.099Z"
        let m = d.getMonth()+1; // get month (0-11) +1 = (1-12) eg. 6
        let y = d.getFullYear(); //get year eg. 2018
        let academicYear = '';
        if(m>=9){
            academicYear = y+'-'+y+1;
        }
        else{
            academicYear = y-1+'-'+y;
        }
        var now = (new Date()).getTime()/1000
        db.ref('AcademicYear/'+academicYear+'/'+this.props.term+"/")

            .on('value', snapshot => {
                snapshot.forEach(element => {
                    let description = element.val()['description']
                    let date = element.val()['startDate']

                    if (description === 'Payment due date' || description === 'Current students’ registration' || description === 'Instruction Begin'
                        || description === "Final examination dates"){
                        if (description === 'Instruction Begin') {
                            this.setState({ instructionBegin: (new Date(date).getTime()/1000)-now, date1: formatDate(new Date(date))})
                        }
                        if (description === 'Payment due date') {
                            this.setState({paymentDueDate: (new Date(date).getTime()/1000)-now,date2: formatDate(new Date(date)) })
                        }
                        if (description === 'Current students’ registration') {
                            this.setState({registration: (new Date(date).getTime()/1000)-now,date3: formatDate(new Date(date)) })
                        }
                        if (description === "Final examination dates"){
                            this.setState({finalExam: (new Date(date).getTime()/1000)-now,date4: formatDate(new Date(date))  })
                        }
                    }
                })
            })
    }

    render(){
        if (this.state.instructionBegin === -1 || this.state.paymentDueDate === -1 || this.state.registration === -1
            || this.state.finalExam === -1){
            return (
                <View style={{ flex: 1 }}>
                    <Spinner visible={true} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
                </View>
            )
        }
        else{
            return (
                <ScrollView>

                    <Count icon={<SimpleLineIcons size={35} color="black" name="cloud-upload"> </SimpleLineIcons>}
                           title={"Registration"}
                           date={this.state.date1}
                           until={this.state.registration}/>

                    <Count icon={<SimpleLineIcons size={35} color="black" name="briefcase"> </SimpleLineIcons>}
                           title={"Instruction Begin"}
                           date={this.state.date2}
                           until={this.state.instructionBegin}/>

                    <Count icon={<SimpleLineIcons size={35} color="black" name="credit-card"> </SimpleLineIcons>}
                           title={"Payment Due Date"}
                           date={this.state.date3}
                           until={this.state.paymentDueDate}/>


                    <Count icon={<SimpleLineIcons size={35} color="black" name="book-open"> </SimpleLineIcons>}
                           title={"Final examination Date"}
                           date={this.state.date4}
                           until={this.state.finalExam}/>

                </ScrollView>
            )
        }

    }
}
