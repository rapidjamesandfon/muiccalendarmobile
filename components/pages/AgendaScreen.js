import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet
} from 'react-native';
import {Agenda} from 'react-native-calendars';
import XDate from 'xdate';
import {firebase, db} from '../../firebase'
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";

export default class AgendaScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: {},
            current: this.props.selectedDate+'-01',
            academicYear: this.props.academicYear,
            todos: {}
        };
    }

    render() {
        return (
            <View style={{height:'100%'}}>

            <Agenda
                items={this.state.items}
                loadItemsForMonth={this.loadItems.bind(this)}
                selected={this.state.current}
                renderItem={this.renderItem.bind(this)}
                renderEmptyDate={this.renderEmptyDate.bind(this)}
                rowHasChanged={this.rowHasChanged.bind(this)}
                minDate={this.state.academicYear.substring(0,4)+'-'+'09'}
                maxDate={this.state.academicYear.substring(5,10)+'-'+'09'}

            />
            </View>
        );
    }
    componentDidMount(){
        db.ref('AcademicYear/' + this.state.academicYear)
            .on('value', snapshot => {
                let temp = {};
                this.snapshotToArray(snapshot).forEach(attr => {
                    if(attr['date']['start'] === attr['date']['end']){
                        if(!temp[attr['date']['start']]){
                            temp[attr['date']['start']]=[]
                        }
                        temp[attr['date']['start']].push(attr);
                    }
                    else{
                        let runningDate = attr['date']['start']
                        while(runningDate !== attr['date']['end']){
                            if(!temp[runningDate]){
                                temp[runningDate]=[]
                            }
                            temp[runningDate].push(attr);
                            runningDate = new XDate(runningDate, true).addDays(1).toString('yyyy-MM-dd');
                        }
                        if(!temp[attr['date']['end']]){
                            temp[attr['date']['end']]=[]
                        }
                        temp[attr['date']['end']].push(attr)
                    }
                });
                this.setState({
                   items : temp,
                });

            });
    }
    snapshotToArray (snapshot) {
        let returnArr = [];
        snapshot.forEach(childSnapshot => {
            childSnapshot.forEach(grandChild => {
                grandChild.val()['date'] = grandChild.val().startDate;
                let temp = {};
                let attr = grandChild.val();
                temp['id'] = grandChild.key;
                temp['color'] = attr.color;
                temp['date'] = {start: attr.startDate, end: attr.endDate};
                temp['name'] = attr.description;
                returnArr.push(temp)
            })
        });
        return returnArr
    }

    loadItems(day) {
            setTimeout(() => {

                for (let i = -365; i < 365; i++) {
                    const time = day.timestamp + i * 24 * 60 * 60 * 1000;
                    const strTime = this.timeToString(time);
                    if (!this.state.items[strTime]) {
                        this.state.items[strTime] = [];
                    }
                }
                const newItems = {};
                Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
                this.setState({
                    items: newItems
                });

            }, 1000);

        }

    renderItemWithIcon(item, icon){
        return (
            <View style={[styles.item, {height: item.height}, {flexDirection: 'row'}]}>
                <View style={{
                    width: 32,
                    height: 32,
                    borderRadius: 32 / 2,
                    backgroundColor: 'transparent',
                    marginRight: 5,
                    justifyContent: 'center'
                }}>
                    <SimpleLineIcons size={30} color={item.color} name={icon}/>
                </View>
                <View style={{justifyContent: 'center'}}>
                    <Text>{item.name}</Text>
                </View>
            </View>
        )
    }

    renderItem(item) {
        if(item.name.indexOf('egistration') !== -1){
            return (
                this.renderItemWithIcon(item, 'cloud-upload')
            )
        }
        else if(item.name.indexOf('nstruction')!==-1){
            return (
                this.renderItemWithIcon(item, 'briefcase')
            )
        }
        else if(item.name.indexOf('ayment')!==-1){
            return (
                this.renderItemWithIcon(item, 'credit-card')
            )
        }
        else if(item.name.indexOf('exam')!==-1){
            return (
                this.renderItemWithIcon(item, 'book-open')
            )
        }
        else if(item.name.indexOf('ithdraw')!==-1){
            return (
                this.renderItemWithIcon(item, 'logout')
            )
        }
        else {
            return (
                <View style={[styles.item, {height: item.height}, {flexDirection: 'row'}]}>
                    <View style={{
                        width: 32,
                        height: 32,
                        borderRadius: 32 / 2,
                        backgroundColor: item.color,
                        marginRight: 5,
                        justifyContent: 'center'
                    }}>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                        <Text>{item.name}</Text>
                    </View>
                </View>
            );
        }
    }

    renderEmptyDate() {
        return (
            <View style={styles.emptyDate}>
                <View style={{height:20}} />
                    <View
                        style={{
                            borderBottomColor: '#eaeaea',
                            borderBottomWidth: 2,
                        }}
                />

            </View>
        );
    }

    rowHasChanged(r1, r2) {
        return r1.name !== r2.name;
    }

    timeToString(time) {
        const date = new Date(time);
        return date.toISOString().split('T')[0];
    }
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17
    },
    emptyDate: {
        height: 15,
        flex:1,
        paddingTop: 30
    }
});