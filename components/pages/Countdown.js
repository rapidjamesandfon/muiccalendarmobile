import * as React from 'react';
import { ScrollView, StyleSheet, Text, View, Dimensions, StatusBar, Image, TouchableOpacity, Animated } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import {  Card  } from 'react-native-elements'
import CountDown from 'react-native-countdown-component';
import RenderPage from "./RenderPage";
import SummerTerm from "./SummerTerm";
import XDate from 'xdate';


export function Count(props) {

    return (

        <Card containerStyle={styles.card}>
            <View style={{flex: 1, flexDirection: 'row', padding: 20,}}>

                {props.icon}

                <View style={{ flexDirection: 'column', justifyContent: 'center'}}>
                    <Text style={{paddingLeft: 10, fontWeight:'bold', color: '#0e4790', fontSize: 14}}>{props.title}</Text>
                    <Text style={{paddingLeft: 10,paddingTop:5, fontWeight:'bold', color: '#545454', fontSize: 12}}>{props.date}</Text>
                </View>
            </View>

                <View style={{position: 'absolute', right: 0, paddingTop:10}}>
                    <CountDown
                        until={props.until}
                        size={20}
                        digitBgColor={'#fcb913'}
                        digitTxtColor={'#000'}
                        timeToShow={['D']}
                    />
                </View>

        </Card>

    );
}

const FirstTrimester = () => (
    <RenderPage term="1"/>
)

const SecondTrimester = () => (
    <RenderPage term="2"/>

);

const ThirdTrimester = () => (
    <RenderPage term="3"/>

);
const Summer = () => (

   <SummerTerm term="Summer"/>

);
export default class Countdown extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'Term 1' },
            { key: 'second', title: 'Term 2' },
            { key: 'third', title: 'Term 3' },
            { key: 'fourth', title: 'Summer' },
        ],


    };

    componentDidMount(){
        let d = XDate(); //current date eg. "2018-06-06T16:54:42.099Z"
        let m = d.getMonth()+1; // get month (0-11) +1 = (1-12) eg. 6
        let term = 0;
        if(m>=9 && m<=12){
            term = 0
        }
        else if (m>= 1 && m<=4){
            term = 1
        }
        else if (m>=4 && m<=7){
            term = 2
        }
        else{
            term = 3
        }
        8
        this.setState({index: term})
    }

    _handleIndexChange = index => this.setState({ index });


    render() {
        return (
            <View>
                <View style={{height:'96%'}}>
                    <View style={{

                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        flexWrap: 'wrap',
                    }}>
                        <TabView
                            key = {0}
                            navigationState={this.state}
                            onIndexChange={index => this.setState({ index })}
                            renderScene={SceneMap({
                                first:FirstTrimester,
                                second:SecondTrimester,
                                third: ThirdTrimester,
                                fourth: Summer
                            })}
                            renderTabBar={props =>
                                <TabBar
                                    {...props}
                                    indicatorStyle={{
                                        backgroundColor: '#fcb913',
                                    }}
                                    style={{
                                        backgroundColor: '#0e4790',
                                        paddingTop: 20
                                    }}
                                />
                            }
                            initialLayout={{ height: 0,
                                width: Dimensions.get('window').width }}
                        />
                    </View>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    card:{
        borderTopWidth: 2,
        borderRadius: 0.5,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#111111',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.4,
        shadowRadius: 1,
        elevation: 1,
    },

});
