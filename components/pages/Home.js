import React from 'react';
import {ScrollView, View, ImageBackground, TextInput, Image, AsyncStorage,TouchableOpacity} from 'react-native';
import {Text} from 'react-native-elements'
import {Actions} from 'react-native-router-flux'
import styles from '../css/HomeCss'
import { Calendar } from 'react-native-calendars';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import XDate from 'xdate';

function MyCalendar(props) {
    let calendarThemeHeight = 16;
    let calendarThemeWidth = 105;
    let textSize = 8.5;
    let monthColor = '#0e4790';
    let cur = XDate();
    console.log();
    let today =cur.toString('yyyy-MM-dd');
    return (
        <TouchableOpacity  onPress={()=> {
            Actions.agendaScreen({selectedDate: props.term+'-'+props.month, academicYear: props.academicYear});
        }
        }>
        <Calendar
            theme={{
                'stylesheet.calendar.main': {
                    week: {
                        marginTop: 0,
                        marginBottom: 0,
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        height: calendarThemeHeight,
                        width: calendarThemeWidth
                    },
                },
                'stylesheet.calendar.header':{
                    header: {

                    },
                    monthText: {
                        marginBottom: 0,
                        fontSize: textSize+5,
                        // fontFamily: 'System',
                        fontWeight: '300',
                        color: monthColor,
                    },
                },
                'stylesheet.day.basic':{
                    base: {
                        width: 32,
                        height: 32,
                        alignItems: 'center'
                    },
                },
                textDayFontSize: textSize,
                textMonthFontSize:textSize+4,
                textDayHeaderFontSize: textSize
            }}
            current={props.term+'-'+props.month}
            onDayPress={(day) => {Actions.agendaScreen({selectedDate: props.term+'-'+props.month, academicYear: props.academicYear})}}
            monthFormat={'MMM'}
            hideArrows={true}
            hideExtraDays={true}
            disableMonthChange={true}
            firstDay={1}
            hideDayNames={true}
            markingType={'custom'}
            markedDates={{
                [today]: {
                    customStyles: {
                        container: {
                            backgroundColor: '#1268e2',
                            height: 16,
                            width: 16,
                            marginBottom: 16,
                            marginLeft: 8,
                            marginRight: 8,
                        },
                        text: {
                            color: 'white',
                            fontWeight: 'bold'
                        },
                    },
                }
            }}
        />
        </TouchableOpacity>
    );
}
function MyCalendarYearly (props){
    let term = props.term;
    let term2 = props.term2;
    let academicYear = props.academicYear;
    return (
        <View style={styles.NavPage}>
            <View style={styles.CalendarYear}>
                <View style={styles.calendarRow}>
                    <MyCalendar term={term} academicYear={academicYear} month={'09'}/>
                    <MyCalendar term={term} academicYear={academicYear} month={'10'}/>
                    <MyCalendar term={term} academicYear={academicYear} month={'11'}/>
                </View>
                <View style={styles.calendarRow}>
                    <MyCalendar term={term} academicYear={academicYear} month={'12'}/>
                    <MyCalendar term={term2} academicYear={academicYear} month={'01'}/>
                    <MyCalendar term={term2} academicYear={academicYear} month={'02'}/>
                </View>
                <View style={styles.calendarRow}>
                    <MyCalendar term={term2} academicYear={academicYear} month={'03'}/>
                    <MyCalendar term={term2} academicYear={academicYear} month={'04'}/>
                    <MyCalendar term={term2} academicYear={academicYear} month={'05'}/>
                </View>
                <View style={styles.calendarRow}>
                    <MyCalendar term={term2} academicYear={academicYear} month={'06'}/>
                    <MyCalendar term={term2} academicYear={academicYear} month={'07'}/>
                    <MyCalendar term={term2} academicYear={academicYear} month={'08'}/>
                </View>
            </View>

        </View>
    );

}
export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTerm : '0'
        }

    }
    componentDidMount(){
        let d = XDate(); //current date eg. "2018-06-06T16:54:42.099Z"
        let m = d.getMonth()+1; // get month (0-11) +1 = (1-12) eg. 6
        let y = d.getFullYear(); //get year eg. 2018
        let academicYear = '';
        if(m>=9){
            academicYear = y+'-'+y+1;
        }
        else{
            academicYear = y-1+'-'+y;
        }
        this.setState({currentTerm: academicYear.substring(0,4)})
    }
    render() {
        let currentTerm = this.state.currentTerm;

        return (
            <View style={{height:'100%'}}>
                <View style={styles.Header}>
                    <TouchableOpacity
                        style={{flexDirection:'row'}}
                        onPress={()=> {
                            this.setState({currentTerm: this.state.currentTerm-1});
                        }

                    }>
                            <Icon style={{paddingTop: 20, paddingLeft:10}}size={20} color="white" name="arrow-left" />
                            <Ionicons style={{paddingTop: 20, paddingLeft:5}}size={20} color="white" name="ios-calendar" />
                    </TouchableOpacity>
                    <View style={styles.ViewYear}><Text style={styles.Year}>{currentTerm}-{1+ +currentTerm}</Text></View>
                    <TouchableOpacity
                        style={{flexDirection:'row'}}
                        onPress={()=> {
                            this.setState({currentTerm: 1+ +this.state.currentTerm});
                        }

                    }>
                        <Ionicons style={{paddingTop: 20, paddingRight:5}} size={20} color="white" name="ios-calendar" />
                        <Icon style={{paddingTop: 20, paddingRight:10}}size={20} color="white" name="arrow-right" />
                    </TouchableOpacity>
                </View>
                <View>
                    <MyCalendarYearly term={currentTerm} term2={1+ +currentTerm} academicYear={(currentTerm)+'-'+(1+ +currentTerm)}/>
                </View>
            </View>
        );
    }
}
