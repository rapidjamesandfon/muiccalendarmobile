import {StyleSheet} from 'react-native'
import colors from "react-native-elements/src/config/colors";

const styles = StyleSheet.create({
    NavPage:{
        // backgroundColor:"#f5f5f5f5",
        // height:"100%"
        backgroundColor: "#ffffff",
        // backgroundColor: "#bb8dd0",
        height:"100%",
        // flex:1
    },
    NavForm:{
        // paddingTop:40,
        paddingLeft:20,
        paddingRight:20,
        // height: 200
        // backgroundColor:"#f5f5f5f5"
    },
    calendarRow:{
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // height:
        paddingBottom: 15
    },
    CalendarYear:{
        // flex: 1,
        height:'100%',
        backgroundColor: "#ffffff",
        padding:15,
    },

    Year:{
        paddingTop: 20,
        textAlign:'center',
        color:'white',
        fontSize: 16
    },
    ViewYear:{


        justifyContent: 'center'
    },
    Header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        backgroundColor:'#0e4790',
        alignItems: 'center',
        height:'10%'
    }
});

export default styles;
