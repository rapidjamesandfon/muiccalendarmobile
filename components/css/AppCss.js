import {StyleSheet} from 'react-native'
import colors from "react-native-elements/src/config/colors";

const styles = StyleSheet.create({
    navBar: {
        // flex: 1,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#0e4790', // changing navbar color
    },




    navTitle: {
        color: 'white', // changing navbar title color
    },
    ButtonNext:{
        backgroundColor: '#000',
        shadowColor: "#000000",
        shadowOpacity: 0.5,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        },
        height:45,
        // width:"100%",
    },
});

export default styles;
