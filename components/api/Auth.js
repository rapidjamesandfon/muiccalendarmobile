import {apiClient} from './MyAxios'



function initRegister(fireToken, cb) {
    const header = { firebaseTok: fireToken, accessTok: faceToken }
    apiClient.post('/register/init', { headers: header })
        .then((res) => {
            cb(true)
        })
        .catch((err) => {
            cb(false)
        })
    }
function initFacebook(fireToken, faceToken, cb) {
        const header = { firebaseTok: fireToken, accessTok: faceToken }
        apiClient.post('/register/init/facebook', { headers: header })
            .then((res) => {
                cb(true)
            })
            .catch((err) => {
                cb(false)
            })
        }
function whoami(fireToken, cb) {
        const header = { firebaseTok: fireToken}
        apiClient.get('/whoami', { headers: header })
            .then((res) => {
                cb(res.data)
            })
            .catch((err) => {
                console.log(err)
                cb(false)
            })
        }

export {initRegister, initFacebook, whoami}

