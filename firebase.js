import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAnhrFewvLURKfRmxfGfVJlLVdwpz86aHY",
    authDomain: "hackathonii-eeeb8.firebaseapp.com",
    databaseURL: "https://hackathonii-eeeb8.firebaseio.com",
    projectId: "hackathonii-eeeb8",
    storageBucket: "hackathonii-eeeb8.appspot.com",
    messagingSenderId: "442692623400"
};

firebase.initializeApp(config);

export default firebase;
export const db = firebase.database();
export const auth = firebase.auth();