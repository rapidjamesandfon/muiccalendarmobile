# Welcome to MUIC Academic Calendar

> A React native project with https://github.com/wix/react-native-calendars
> Android and iOS compatible.

MUIC Academic Calendar is a project that turn a static academic calendar to a dynamic calendar with event detail and countdown on important event.
Where admin could add event by signing in (https://hackathonii-eeeb8.firebaseapp.com/admin) and delete event in /adminDelete.

This is mobile application that give the detail of the event happening in any school. There is a countdown on important event that no one should miss out!

## Prerequisites
* yarn
* xcode
* watchman
* firebase

``` bash
# install dependencies
yarn

# to start up the simulator
yarn run ios
```

## Demo
Screenshot available in /Demo


By Kanokporn Pringpayong (Fon) and Sorakris Chaladlamsakul (James)

Enjoy

