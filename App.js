import React from 'react';
import {Router, Scene} from 'react-native-router-flux'
import Home from './components/pages/Home'
import styles from './components/css/AppCss.js'
import {View, StatusBar} from 'react-native'
import Countdown from "./components/pages/Countdown";
import AgendaScreen from "./components/pages/AgendaScreen";
import ListAll from "./components/pages/ListAll";
import Main from "./components/pages/Main";

console.disableYellowBox = true;
export default class App extends React.Component {
    render() {
        return (
            <View style={{height:"100%"}}>
                <StatusBar barStyle="light-content"  />

                <Router>
                    <Scene key="root">

                        <Scene key="countdown" component={Countdown} title="Countdown" titleStyle={styles.navTitle} navigationBarStyle={styles.navBar} hideNavBar/>
                        <Scene key="home" component={Home} title="Home" titleStyle={styles.navTitle} navigationBarStyle={styles.navBar} hideNavBar />
                        <Scene key="agendaScreen" component={AgendaScreen} title="Agenda" titleStyle={styles.navTitle} navigationBarStyle={styles.navBar} hideNavBar  />
                        <Scene key="listall" component={ListAll}  hideNavBar   />
                        <Scene key="main" component={Main}  navigationBarStyle={styles.navBar}  hideNavBar  initial/>

                    </Scene>
                </Router>
            </View>
        );
    }
}
